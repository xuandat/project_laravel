$(document).ready(function () {

    function notify(noti) {
        $('.noti-success').append('<div class="noti animate__animated animate__zoomInRight"\n' +
            'style="font-weight: bold; font-size: 20px; color:red;">' + noti + '</div>');
        $(".noti").delay(3000).slideUp();
    }

    // create student
    $(document).on('click', '.add-student', function () {
        let url = $(this).attr('data-url');
        let formData = new FormData($('#form-add-student')[0]);
        console.log(formData);
        callAjax(url, 'POST', formData)
            .then(function (data) {
                $('.data-table').load(' .data');
                $('#modal-add-student').modal('hide');
                $('.paginate').load(' .datas');
                notify('Create new student successfully!!');
            });
    });

    //detail-student
    $(document).on('click', '.btn-detail-student', function () {
        $('#detail-student').modal('show');
        var url = $(this).attr('data-url');
        callAjax(url, 'GET')
            .then(function (data) {
                loadViewStudent(data);
            });
    });

    //show edit student
    $(document).on('click', '.btn-edit-student', function () {
        $('#modal-edit-student').modal('show');
        var url = $(this).attr('data-url');
        $('.edit-student').attr('data-url', $(this).attr('data-action'));
        callAjax(url, 'GET')
            .then(function (data) {
                fillDataStudent(data);
            });
    });

    //update
    $(document).on('click', '.edit-student', function () {
        let formData = new FormData($('#form-edit-student')[0]);
        var url = $(this).attr('data-url');
        $('#modal-edit-student').modal('hide');
        callAjax(url, 'POST', formData)
            .then(function (data) {
                console.log(data);
                $('.modal-loads').load(' .avatar-views');
                $('.data-table').load(' .data');
                $('.paginate').load(' .datas');
                notify('Update student successfully!!');
            });
    });

    //delete
    // delete student
    $(document).on('click', '.btn-delete-student', function () {
        let url = $(this).attr('data-url');
        $('#modal-delete-student').modal('show');
        $('.delete-student').attr('data-url', url);
    });

    $(document).on('click', '.delete-student', function () {
        let url = $(this).attr('data-url');
        $('#modal-delete-student').modal('hide');
        $('.delete-student').attr('data-url', url);
        callAjax(url, 'POST')
            .then(function (data) {
                $('.data-table').load(' .data');
                $('.paginate').load(' .datas');
                notify('Deleted student successfully!!');
            });
    });

    // load view student
    function loadViewStudent(response) {
        let gender, status;
        //check gender
        switch (response.student.gender) {
            case 1 :
                gender = 'nam';
                break;
            case 2 :
                gender = 'nữ';
                break;
            case 3 :
                gender = '3D';
                break;
        }
        //check status
        response.student.status == 1 ? status = "Đang học" : status = "Nghỉ học";
        $('#name-view').html(response.student.name);
        $('#gender-view').html(gender);
        $('#birthday-view').html(response.student.birthday);
        $('#address-view').html(response.student.address);
        $('#phone-view').html(response.student.phone);
        $('#status-view').html(status);
        $('#faculty-view').html(response.student.faculty.name);
        $('.avatar-view').append(
            '<img style="border-radius: 5px; box-shadow: 3px 3px 7px grey; width: 150px ;height: 150px; margin: 15px" '
            + 'src="http://quanlysinhvien.com/assets/uploads/students/' + response.student.avatar + '">'
        );

    }


    function fillDataStudent(response) {
        var faculty_id = response.student.faculty_id;
        var gender = response.student.gender;
        $('#faculty_id' + faculty_id).attr('selected', 'true');
        $('#name_edit').val(response.student.name);
        $('#birthday_edit').val(response.student.birthday);
        $('#address_edit').val(response.student.address);
        $('#phone_edit').val(response.student.phone);
        $('#avatar_current').val(response.student.avatar);
        $('.avatar_views').append(
            '<img style="border-radius: 5px; box-shadow: 3px 3px 7px grey; width: 150px ;height: 150px; margin: 15px" '
            + 'src="http://quanlysinhvien.com/assets/uploads/students/' + response.student.avatar + '">'
        );
        $('input[name=gender][value=' + gender + ']').attr('checked', true);
    }

    // show avatar
    $("#avatar").change(function (e) {
        e.preventDefault();
        $('#image').attr({
            'width': '150',
            'height': '150'
        });
        var reader = new FileReader();
        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("image").src = e.target.result;
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });

    $('#avatar_new').change(function (e) {
        e.preventDefault();
        $('#images').attr({
            'width': '150',
            'height': '150'
        });
        var reader = new FileReader();
        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("images").src = e.target.result;
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    });

    $(".btn-close-modal").click(function () {
        $('.modal-load').load(' .avatar-view');
        $('.modal-loads').load(' .avatar-views');
        $('.view-image').load('img');
    });
});

function validateAdd() {
    var name = $('#name').val();
    var avatar = $('#avatar').val();
    var birthday = $('#birthday').val();
    var address = $('#address').val();
    var phone = $('#phone').val();
    $('#name').keyup(function (e) {
        if (name != null) {
            $('#errorName').html('');
        }
    });
    $('#avatar').change(function (e) {
        if (avatar != null) {
            $('#errorAvatar').html('');
        }
    });
    $('#birthday').keyup(function (e) {
        if (birthday != null) {
            $('#errorBirthday').html('');
        }
    });
    $('#address').keyup(function (e) {
        if (address != null) {
            $('#errorAddress').html('');
        }
    });
    $('#phone').keyup(function (e) {
        if (phone != null) {
            $('#errorPhone').html('');
        }
    });
    var error = "Not yet valid !";
    if (name == '' || name == null) {
        $('#errorName').html(error);
        $('#name').focus();
    } else if (avatar == '' || avatar == null) {
        $('#errorAvatar').html(error);
        $('#avatar').focus();
    } else if (birthday == '' || birthday == null) {
        $('#errorBirthday').html(error);
        $('#birthday').focus();
    } else if (address == '' || address == null) {
        $('#errorAddress').html(error);
        $('#address').focus();
    } else if (phone == '' || phone == null) {
        $('#errorPhone').html(error);
        $('#phone').focus();
    }
    return true;
}

