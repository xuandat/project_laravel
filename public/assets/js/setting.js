$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function callAjax(url, method, data) {
    return $.ajax({
        type: method,
        url: url,
        data: data,
        processData: false,
        contentType: false,
    });
}
