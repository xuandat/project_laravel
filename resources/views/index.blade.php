<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.layouts.head')
</head>

<body id="page-top">
    <div id="wrapper">
        <!-- Sidebar -->
        @include('admin.layouts.side-bar')
        <!-- Sidebar -->
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <!-- TopBar -->
                @include('admin.layouts.top-bar')
                <!-- Topbar -->

                <!-- Container Fluid-->
                <div class="container-fluid" id="container-wrapper">
                    @yield('content')
                </div>
                <!---Container Fluid-->
            </div>
            <!-- Footer -->
            @include('admin.layouts.footer')
            <!-- Footer -->
        </div>
    </div>
    <!-- Scroll to top -->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    @include('admin.layouts.script')
    @yield('script')
</body>

</html>
