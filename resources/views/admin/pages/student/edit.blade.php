<div class="modal fade" id="modal-edit-student" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenterTitle">Thêm sinh viên mới</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="POST" id="form-edit-student" enctype="multipart/form-data" >
                    @csrf
                    <div class="form-group">
                        <label for="name">Tên sinh viên : <span style="color:red;font-weight: bold" id="errorName-edit"></span></label>
                        <input type="text" class="form-control" id="name_edit" name="name"
<<<<<<< HEAD:resources/views/admin/pages/student/edit.blade.php
                               placeholder="Nhập tên sinh viên">
=======

                            placeholder="Nhập tên sinh viên">
>>>>>>> 7201fec9505dfa660ab79ed1c2b892425f18978f:resources/views/pages/student/edit-student.blade.php
                    </div>
                    <div class="form-group">
                        <div class="modal-loads">
                            <div class="avatar_views">

                            </div>
                        </div>
                    </div>
<<<<<<< HEAD:resources/views/admin/pages/student/edit.blade.php
                    <div class="form-group">
                        <label for="avatar">Update avatar</label>
                        <input type="file" class="form-control" id="avatar_new" name="avatar">
=======
                    <input type="text" class="form-control" id="avatar_current" name="avatar_current"
                        style="display: none;">
                    <div class="form-group">
                        <label for="avatar">Cập nhật ảnh đại diện</label>
                        <input type="file" accept="image/*" class="form-control" id="avatar_new" name="avatar_new">
                        <img id="images" style="margin: 10px; border:5px; box-shadow: 3px 3px 7px grey"/>
>>>>>>> 7201fec9505dfa660ab79ed1c2b892425f18978f:resources/views/pages/student/edit-student.blade.php
                    </div>
                    <div class="form-group">
                        <label for="faculty_id">Faculty</label>
                        <select name="faculty_id" class="form-control" id="">
                            @foreach ($faculties as $faculty)
                                <option id="faculty_id{{ $faculty->id }}" value="{{ $faculty->id }}">
                                    {{ $faculty->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
<<<<<<< HEAD:resources/views/admin/pages/student/edit.blade.php
                        <label for="birthday">Date of birth</label>
                        <input type="date" class="form-control" id="birthday_edit" name="birthday">
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" class="form-control" id="address_edit" name="address"
                               placeholder="Nhập tên địa chỉ...">
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input type="text" class="form-control" id="phone_edit" name="phone"
                               placeholder="Nhập số điện thoại...">
=======
                        <label for="birthday">Ngày sinh : <span style="color:red;font-weight: bold" id="errorBirthday-edit"></span></label>
                        <input type="date" class="form-control" id="birthday-edit" name="birthday">
                    </div>
                    <div class="form-group">
                        <label for="address">Địa chỉ : <span style="color:red;font-weight: bold" id="errorAddress-edit"></span></label>
                        <input type="text" class="form-control" id="address-edit" name="address"
                            placeholder="Nhập tên địa chỉ...">
                    </div>
                    <div class="form-group">
                        <label for="phone">Số điện thoại :<span style="color:red;font-weight: bold" id="errorPhone-edit"></span></label>
                        <input type="text" class="form-control" id="phone-edit" name="phone"
                            placeholder="Nhập số điện thoại...">
>>>>>>> 7201fec9505dfa660ab79ed1c2b892425f18978f:resources/views/pages/student/edit-student.blade.php
                    </div>
                    <div class="form-group">
                        <label for="gender">Giới tính</label><br>
                        <input type="radio" style="margin-left:10px" value="1" name="gender"> Nam
                        <input type="radio" style="margin-left:10px" value="2" name="gender"> Nữ
                        <input type="radio" style="margin-left:10px" value="3" name="gender"> Khác
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary edit-student">Update</button>
                <button type="button" class="btn btn-outline-primary btn-close-modal"
                        data-dismiss="modal">Close
                </button>
            </div>
        </div>
    </div>
</div>
