<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Auth::routes();

Route::middleware('auth')->group(function () {

    Route::get('/', 'HomeController@index')->name('home');

    // FACULTY
    Route::prefix('faculties')->group(function () {
        // index
        Route::get('/', 'FacultyController@index')
            ->name('faculties.index');

        //Create
        Route::get('/create', 'FacultyController@create')
            ->name('faculties.create');
        Route::post('/create', 'FacultyController@store')
            ->name('faculties.store');

        //Update
        Route::get('/edit/{id}', 'FacultyController@edit')
            ->name('faculties.edit');
        Route::post('/edit/{id}', 'FacultyController@update')
            ->name('faculties.update');

        //Show
        Route::get('/show/{id}', 'FacultyController@show')
            ->name('faculties.show');
        Route::get('/list', 'FacultyController@list')
            ->name('faculties.list');

        //Delete
        Route::post('/delete/{id}', 'FacultyController@destroy')
            ->name('faculties.delete');

    });

    //STUDENT
    Route::prefix('students')->group(function () {

        // index
        Route::get('/', 'StudentController@index')
            ->name('students.index');

        //Create
        Route::get('/create', 'StudentController@create')
            ->name('students.create');
        Route::post('/create', 'StudentController@store')
            ->name('students.store');

        //Update
        Route::get('/edit/{id}', 'StudentController@edit')
            ->name('students.edit');
        Route::post('/edit/{id}', 'StudentController@update')
            ->name('students.update');

        //Show
        Route::get('/show/{id}', 'StudentController@show')
            ->name('students.show');

        //Delete
        Route::post('/delete/{id}', 'StudentController@destroy')
            ->name('students.delete');
    });
});




