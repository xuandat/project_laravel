<?php

namespace App\Repositories\Student;

use App\Models\Student;
use App\Repositories\BaseRepository;

/**
 *
 */
class StudentRepository extends BaseRepository
{

    protected function model(): string
    {
        return Student::class;

    }

    public function getWithPaginate($quantity = 5)
    {
        return $this->model->latest('id')->paginate($quantity);
    }

}
