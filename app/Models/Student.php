<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @method static where(string $string, int $id)
 * @method static findOrFail(int $id)
 * @method static create(array $data)
 */
class Student extends Model
{
    protected $table = "students";

    protected $fillable = [
        'faculty_id', 'name', 'avatar', 'birthday', 'address', 'phone', 'gender', 'status'
    ];

    public function faculty(): BelongsTo
    {
        return $this->belongsTo(Faculty::class);
    }
}
