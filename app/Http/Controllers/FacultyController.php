<?php

namespace App\Http\Controllers;

use App\Http\Requests\Faculty\StoreFacultyRequest;
use App\Models\Faculty;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Repositories\Faculty\FacultyRepository;
use Illuminate\Http\Response;
use Illuminate\View\View;

class FacultyController extends Controller
{
    protected $facultyRepository;

    public function __construct(FacultyRepository $facultyRepository)
    {
        return $this->facultyRepository = $facultyRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $faculties = $this->facultyRepository->getWithPaginate(5);
        return view('admin.pages.faculty.index', compact('faculties'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(): Response
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFacultyRequest $request
     * @return JsonResponse
     */
    public function store(StoreFacultyRequest $request): JsonResponse
    {
        $faculty = $this->facultyRepository->create($request->all());
        return response()->json([
            'faculty' => $faculty,
            'message' => 'Add faculty successfully',
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @return void
     */
    public function show(Request $request)
    {
//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function edit(int $id): JsonResponse
    {
        $faculty = Faculty::findOrFail($id);
        return response()->json([
            'faculty' => $faculty,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreFacultyRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(StoreFacultyRequest $request, int $id): JsonResponse
    {
        $faculty = Faculty::findOrFail($id)->update($request->all());
        return response()->json([
            'faculty' => $faculty,
            'message' => 'Updated successfully',
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {

        Faculty::findOrFail($id)->delete();
        return response()->json([
            'message' => 'Deleted successfully',
        ], 200);
    }
}
